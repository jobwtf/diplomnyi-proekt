Сортировка выбором (Selection sort) — алгоритм сортировки. Может быть как устойчивый, так и неустойчивый. На массиве из n элементов имеет время выполнения в худшем, среднем и лучшем случае OАлгоритм без дополнительного выделения памяти

Алгоритм без дополнительного выделения памяти
Шаги алгоритма:
находим номер минимального значения в текущем списке
производим обмен этого значения со значением первой неотсортированной позиции (обмен не нужен, если минимальный элемент уже находится на данной позиции)
теперь сортируем хвост списка, исключив из рассмотрения уже отсортированные элементы
Для реализации устойчивости алгоритма необходимо в пункте 2 минимальный элемент непосредственно вставлять в первую неотсортированную позицию, не меняя порядок остальных элементов.(n²), предполагая что сравнения делаются за постоянное время.