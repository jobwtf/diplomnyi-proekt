def selection_sort(ar):
    length = len(ar)
    for i in range(length)):
        x = i
        for z in range(x + 1, length)):
            if ar[x] > ar[z]:
                x = z
        ar[i], ar[x] = ar[x], ar[i]
    return ar