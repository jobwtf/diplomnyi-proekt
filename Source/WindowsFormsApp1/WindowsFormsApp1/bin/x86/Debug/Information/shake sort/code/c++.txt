 #include <algorithm>

  template< typename Iterator >
  void cocktail_sort( Iterator first, Iterator last )
  {
      for( --last; first < last; --last, ++first )
      {
          for( Iterator i = first; i < last; ++i )
              if ( *(i + 1) < *i )
                  std::iter_swap( i, i + 1 );

          for( Iterator i = last - 1; i > first; --i )
              if ( *i < *(i - 1) )
                  std::iter_swap( i, i - 1 );
      }
  }