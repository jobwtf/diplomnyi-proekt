№1
/**
* @brief         Сортировка элементов от left до right массива buf
* @param[in/out] buf - сортируемый массив
* @param[in]     left - левая граница. При первой итерации left = 0
* @param[in]     right - правая граница. При первой итерации right = buf.size() - 1
*
* В результате сортируются все элементы массива buf от left до right включительно.
*/
template<typename Type>
void MergeSort(vector<Type>& buf, size_t left, size_t right)
{
    //! Условие выхода из рекурсии
    if(left >= right) return;

    size_t middle = left + (right - left) / 2;

    //! Рекурсивная сортировка полученных массивов
    MergeSort(buf, left, middle);
    MergeSort(buf, middle + 1, right);
    merge(buf, left, right, middle);
}

/**
 * @brief Слияние элементов.
 * @param[in/out] buf - массив
 * @param[in]     left - левая граница. При певой итерации left = 0
 * @param[in]     right - правая граница. При первой итерации right = buf.size() - 1
 * @param[in]     middle - граница подмассивов.
 *
 * Массив buf содержит два отсортированных подмассива:
 * - [left; middle] - первый отсортированный подмассив.
 * - [middle+1; right] - второй отсортированный подмассив.
 * В результате получаем отсортированный массив, полученный из двух подмассивов,
 * который сохраняется в buf[left; right].
 */
template<typename Type>
static void merge(vector<Type>& buf, size_t left, size_t right, size_t middle)
{
    if (left >= right || middle < left || middle > right) return;
    if (right == left + 1 && buf[left] > buf[right]) {
        swap(buf[left], buf[right]);
        return;
    }

    vector<Type> tmp(&buf[left], &buf[right] + 1));

    for (size_t i = left, j = 0, k = middle - left + 1; i <= right; ++i) {
        if (j > middle - left) {      
            buf[i] = tmp[k++];
        } else if(k > right - left) {
            buf[i] = tmp[j++];
        } else {
            buf[i] = (tmp[j] < tmp[k]) ? tmp[j++] : tmp[k++];
        }
    }
}
№2также итеративный алгоритм сортировки, избавленный от рекурсивных вызовов. Такой алгоритм называют «Восходящей сортировкой слиянием».
/*
* Слияние двух упорядоченных массивов
* m1 - Первый массив
* m2 - Второй массив
* len_1 - Длина первого массива
* len_2 - Длина второго массива
* Возвращает объединённый массив
*/
template <class T>
T* merge(T *m1, T* m2, int len_1, int len_2)
{
    T* ret = new T[len_1+len_2];
    int n = 0;
    // Сливаем массивы, пока один не закончится
    while (len_1 && len_2) {
        if (*m1 < *m2) {
            ret[n] = *m1;
            m1++;
            --len_1;
        } else {
            ret[n] = *m2;
            ++m2;
            --len_2;
        }
        ++n;
    }
    // Если закончился первый массив
    if (len_1 == 0) {
        for (int i = 0; i < len_2; ++i) {
            ret[n++] = *m2++;
        }
    } else { // Если закончился второй массив
        for (int i = 0; i < len_1; ++i) {
            ret[n++] = *m1++;
        }
    }
    return ret;
}

// Функция восходящего слияния
template <class T>
void mergeSort(T * mas, int len)
{
    int n = 1, k, ost;
    T * mas1;
    while (n < len) {
        k = 0;
        while (k < len) {
            if (k + n >= len) break;
            ost = (k + n * 2 > len) ? (len - (k + n)) : n;
            mas1 = merge(mas + k, mas + k + n, n, ost);
            for (int i = 0; i < n + ost; ++i)
                mas[k+i] = mas1[i];//тут нужно что-то поменять, потому что это лишнее копирование, и оно увеличивает время работы алгоритма в два раза
            delete [] mas1;
            k += n * 2;
        }
        n *= 2;
    }
}