﻿namespace WindowsFormsApp1
{
    partial class Compare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title2 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title3 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea4 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title4 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title5 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title6 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Compare));
            this.button_generation = new System.Windows.Forms.Button();
            this.button_start_sort = new System.Windows.Forms.Button();
            this.textBoxSourceArray = new System.Windows.Forms.TextBox();
            this.chart_buble = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_Shake = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.checkBoxBuble = new System.Windows.Forms.CheckBox();
            this.checkBoxShake = new System.Windows.Forms.CheckBox();
            this.numericUpDownElement = new System.Windows.Forms.NumericUpDown();
            this.labelCountElement = new System.Windows.Forms.Label();
            this.trackBarSpedAnim = new System.Windows.Forms.TrackBar();
            this.chart_insert = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.checkBoxInsert = new System.Windows.Forms.CheckBox();
            this.checkBoxSelect = new System.Windows.Forms.CheckBox();
            this.chart_select = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart_merge = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.checkBoxMerge = new System.Windows.Forms.CheckBox();
            this.chart_shell = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.checkBoxShell = new System.Windows.Forms.CheckBox();
            this.checkBoxSpeedAnim = new System.Windows.Forms.CheckBox();
            this.groupBoxSort = new System.Windows.Forms.GroupBox();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.checkedListBoxGenerationArray = new System.Windows.Forms.CheckedListBox();
            this.labelGenerationArray = new System.Windows.Forms.Label();
            this.ComparemenuStrip = new System.Windows.Forms.MenuStrip();
            this.FileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fAQToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.labelSourceArray = new System.Windows.Forms.Label();
            this.labelResultSort = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart_buble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_Shake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownElement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpedAnim)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_insert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_select)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_merge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_shell)).BeginInit();
            this.groupBoxSort.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            this.ComparemenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_generation
            // 
            this.button_generation.Location = new System.Drawing.Point(93, 243);
            this.button_generation.Name = "button_generation";
            this.button_generation.Size = new System.Drawing.Size(75, 23);
            this.button_generation.TabIndex = 7;
            this.button_generation.Text = "Генерация массива";
            this.button_generation.UseVisualStyleBackColor = true;
            this.button_generation.Click += new System.EventHandler(this.button__generation_Click);
            // 
            // button_start_sort
            // 
            this.button_start_sort.Location = new System.Drawing.Point(12, 243);
            this.button_start_sort.Name = "button_start_sort";
            this.button_start_sort.Size = new System.Drawing.Size(75, 23);
            this.button_start_sort.TabIndex = 6;
            this.button_start_sort.Text = "Сортировка";
            this.button_start_sort.UseVisualStyleBackColor = true;
            this.button_start_sort.Click += new System.EventHandler(this.button__start_sort_Click);
            // 
            // textBoxSourceArray
            // 
            this.textBoxSourceArray.Location = new System.Drawing.Point(12, 54);
            this.textBoxSourceArray.Multiline = true;
            this.textBoxSourceArray.Name = "textBoxSourceArray";
            this.textBoxSourceArray.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxSourceArray.Size = new System.Drawing.Size(213, 79);
            this.textBoxSourceArray.TabIndex = 5;
            this.textBoxSourceArray.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxSourceArray_KeyPress);
            // 
            // chart_buble
            // 
            this.chart_buble.BackSecondaryColor = System.Drawing.Color.White;
            chartArea1.AxisX.MajorGrid.Enabled = false;
            chartArea1.AxisX.MajorTickMark.Enabled = false;
            chartArea1.AxisX.ToolTip = "Количество элементов";
            chartArea1.AxisY.MajorGrid.Enabled = false;
            chartArea1.AxisY.MajorTickMark.Enabled = false;
            chartArea1.Name = "ChartArea1";
            this.chart_buble.ChartAreas.Add(chartArea1);
            this.chart_buble.Location = new System.Drawing.Point(245, 38);
            this.chart_buble.Name = "chart_buble";
            this.chart_buble.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            series1.YValuesPerPoint = 4;
            this.chart_buble.Series.Add(series1);
            this.chart_buble.Size = new System.Drawing.Size(513, 111);
            this.chart_buble.TabIndex = 8;
            this.chart_buble.Text = "chart1";
            title1.Name = "Title_Buble";
            title1.Text = "Сортировка пузырьком";
            this.chart_buble.Titles.Add(title1);
            // 
            // chart_Shake
            // 
            this.chart_Shake.BackSecondaryColor = System.Drawing.Color.White;
            chartArea2.AxisX.MajorGrid.Enabled = false;
            chartArea2.AxisX.MajorTickMark.Enabled = false;
            chartArea2.AxisX.ToolTip = "Количество элементов";
            chartArea2.AxisY.MajorGrid.Enabled = false;
            chartArea2.AxisY.MajorTickMark.Enabled = false;
            chartArea2.Name = "ChartArea1";
            this.chart_Shake.ChartAreas.Add(chartArea2);
            this.chart_Shake.Location = new System.Drawing.Point(245, 155);
            this.chart_Shake.Name = "chart_Shake";
            this.chart_Shake.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chart_Shake.Series.Add(series2);
            this.chart_Shake.Size = new System.Drawing.Size(513, 111);
            this.chart_Shake.TabIndex = 9;
            this.chart_Shake.Text = "ChartShake";
            title2.Name = "ShakeSort";
            title2.Text = "Шейкерная сортировка";
            this.chart_Shake.Titles.Add(title2);
            // 
            // textBoxResult
            // 
            this.textBoxResult.Location = new System.Drawing.Point(12, 156);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxResult.Size = new System.Drawing.Size(213, 79);
            this.textBoxResult.TabIndex = 10;
            // 
            // checkBoxBuble
            // 
            this.checkBoxBuble.AutoSize = true;
            this.checkBoxBuble.Location = new System.Drawing.Point(6, 19);
            this.checkBoxBuble.Name = "checkBoxBuble";
            this.checkBoxBuble.Size = new System.Drawing.Size(146, 17);
            this.checkBoxBuble.TabIndex = 11;
            this.checkBoxBuble.Text = "Сортировка пузырьком";
            this.checkBoxBuble.UseVisualStyleBackColor = true;
            // 
            // checkBoxShake
            // 
            this.checkBoxShake.AutoSize = true;
            this.checkBoxShake.Location = new System.Drawing.Point(6, 42);
            this.checkBoxShake.Name = "checkBoxShake";
            this.checkBoxShake.Size = new System.Drawing.Size(145, 17);
            this.checkBoxShake.TabIndex = 12;
            this.checkBoxShake.Text = "Шейкерная сортировка";
            this.checkBoxShake.UseVisualStyleBackColor = true;
            // 
            // numericUpDownElement
            // 
            this.numericUpDownElement.Location = new System.Drawing.Point(136, 16);
            this.numericUpDownElement.Name = "numericUpDownElement";
            this.numericUpDownElement.Size = new System.Drawing.Size(61, 20);
            this.numericUpDownElement.TabIndex = 13;
            this.numericUpDownElement.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // labelCountElement
            // 
            this.labelCountElement.AutoSize = true;
            this.labelCountElement.Location = new System.Drawing.Point(6, 23);
            this.labelCountElement.Name = "labelCountElement";
            this.labelCountElement.Size = new System.Drawing.Size(124, 13);
            this.labelCountElement.TabIndex = 14;
            this.labelCountElement.Text = "Количество элементов";
            // 
            // trackBarSpedAnim
            // 
            this.trackBarSpedAnim.AutoSize = false;
            this.trackBarSpedAnim.LargeChange = 500;
            this.trackBarSpedAnim.Location = new System.Drawing.Point(6, 65);
            this.trackBarSpedAnim.Maximum = 1000;
            this.trackBarSpedAnim.Name = "trackBarSpedAnim";
            this.trackBarSpedAnim.Size = new System.Drawing.Size(191, 28);
            this.trackBarSpedAnim.SmallChange = 500;
            this.trackBarSpedAnim.TabIndex = 15;
            this.trackBarSpedAnim.TickFrequency = 500;
            // 
            // chart_insert
            // 
            this.chart_insert.BackSecondaryColor = System.Drawing.Color.White;
            chartArea3.AxisX.MajorGrid.Enabled = false;
            chartArea3.AxisX.MajorTickMark.Enabled = false;
            chartArea3.AxisX.ToolTip = "Количество элементов";
            chartArea3.AxisY.MajorGrid.Enabled = false;
            chartArea3.AxisY.MajorTickMark.Enabled = false;
            chartArea3.Name = "ChartArea1";
            this.chart_insert.ChartAreas.Add(chartArea3);
            this.chart_insert.Location = new System.Drawing.Point(245, 272);
            this.chart_insert.Name = "chart_insert";
            this.chart_insert.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series3.ChartArea = "ChartArea1";
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.chart_insert.Series.Add(series3);
            this.chart_insert.Size = new System.Drawing.Size(513, 111);
            this.chart_insert.TabIndex = 17;
            this.chart_insert.Text = "ChartInsert";
            title3.Name = "InsertSort";
            title3.Text = "Сортировка вставками";
            this.chart_insert.Titles.Add(title3);
            // 
            // checkBoxInsert
            // 
            this.checkBoxInsert.AutoSize = true;
            this.checkBoxInsert.Location = new System.Drawing.Point(6, 65);
            this.checkBoxInsert.Name = "checkBoxInsert";
            this.checkBoxInsert.Size = new System.Drawing.Size(144, 17);
            this.checkBoxInsert.TabIndex = 18;
            this.checkBoxInsert.Text = "Сортировка вставками";
            this.checkBoxInsert.UseVisualStyleBackColor = true;
            // 
            // checkBoxSelect
            // 
            this.checkBoxSelect.AutoSize = true;
            this.checkBoxSelect.Location = new System.Drawing.Point(6, 88);
            this.checkBoxSelect.Name = "checkBoxSelect";
            this.checkBoxSelect.Size = new System.Drawing.Size(135, 17);
            this.checkBoxSelect.TabIndex = 20;
            this.checkBoxSelect.Text = "Сортировка выбором";
            this.checkBoxSelect.UseVisualStyleBackColor = true;
            // 
            // chart_select
            // 
            this.chart_select.BackSecondaryColor = System.Drawing.Color.White;
            chartArea4.AxisX.MajorGrid.Enabled = false;
            chartArea4.AxisX.MajorTickMark.Enabled = false;
            chartArea4.AxisX.ToolTip = "Количество элементов";
            chartArea4.AxisY.MajorGrid.Enabled = false;
            chartArea4.AxisY.MajorTickMark.Enabled = false;
            chartArea4.Name = "ChartArea1";
            this.chart_select.ChartAreas.Add(chartArea4);
            this.chart_select.Location = new System.Drawing.Point(245, 389);
            this.chart_select.Name = "chart_select";
            this.chart_select.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series4.ChartArea = "ChartArea1";
            series4.Legend = "Legend1";
            series4.Name = "Series1";
            this.chart_select.Series.Add(series4);
            this.chart_select.Size = new System.Drawing.Size(513, 111);
            this.chart_select.TabIndex = 21;
            this.chart_select.Text = "ChartSelect";
            title4.Name = "SelectSort";
            title4.Text = "Сортировка выбором";
            this.chart_select.Titles.Add(title4);
            // 
            // chart_merge
            // 
            this.chart_merge.BackSecondaryColor = System.Drawing.Color.White;
            chartArea5.AxisX.MajorGrid.Enabled = false;
            chartArea5.AxisX.MajorTickMark.Enabled = false;
            chartArea5.AxisX.ToolTip = "Количество элементов";
            chartArea5.AxisY.MajorGrid.Enabled = false;
            chartArea5.AxisY.MajorTickMark.Enabled = false;
            chartArea5.Name = "ChartArea1";
            this.chart_merge.ChartAreas.Add(chartArea5);
            this.chart_merge.Location = new System.Drawing.Point(245, 506);
            this.chart_merge.Name = "chart_merge";
            this.chart_merge.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series5.ChartArea = "ChartArea1";
            series5.Legend = "Legend1";
            series5.Name = "Series1";
            this.chart_merge.Series.Add(series5);
            this.chart_merge.Size = new System.Drawing.Size(513, 111);
            this.chart_merge.TabIndex = 22;
            this.chart_merge.Text = "ChartMerge";
            title5.Name = "SelectSort";
            title5.Text = "Сортировка слиянием";
            this.chart_merge.Titles.Add(title5);
            // 
            // checkBoxMerge
            // 
            this.checkBoxMerge.AutoSize = true;
            this.checkBoxMerge.Location = new System.Drawing.Point(6, 111);
            this.checkBoxMerge.Name = "checkBoxMerge";
            this.checkBoxMerge.Size = new System.Drawing.Size(139, 17);
            this.checkBoxMerge.TabIndex = 23;
            this.checkBoxMerge.Text = "Сортировка слиянием";
            this.checkBoxMerge.UseVisualStyleBackColor = true;
            // 
            // chart_shell
            // 
            this.chart_shell.BackSecondaryColor = System.Drawing.Color.White;
            chartArea6.AxisX.MajorGrid.Enabled = false;
            chartArea6.AxisX.MajorTickMark.Enabled = false;
            chartArea6.AxisX.ToolTip = "Количество элементов";
            chartArea6.AxisY.MajorGrid.Enabled = false;
            chartArea6.AxisY.MajorTickMark.Enabled = false;
            chartArea6.Name = "ChartArea1";
            this.chart_shell.ChartAreas.Add(chartArea6);
            this.chart_shell.Location = new System.Drawing.Point(245, 623);
            this.chart_shell.Name = "chart_shell";
            this.chart_shell.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Fire;
            series6.ChartArea = "ChartArea1";
            series6.Legend = "Legend1";
            series6.Name = "Series1";
            this.chart_shell.Series.Add(series6);
            this.chart_shell.Size = new System.Drawing.Size(513, 111);
            this.chart_shell.TabIndex = 24;
            this.chart_shell.Text = "ChartShell";
            title6.Name = "ShellSort";
            title6.Text = "Сортировка Шелла";
            this.chart_shell.Titles.Add(title6);
            // 
            // checkBoxShell
            // 
            this.checkBoxShell.AutoSize = true;
            this.checkBoxShell.Location = new System.Drawing.Point(6, 134);
            this.checkBoxShell.Name = "checkBoxShell";
            this.checkBoxShell.Size = new System.Drawing.Size(122, 17);
            this.checkBoxShell.TabIndex = 25;
            this.checkBoxShell.Text = "Сортировка Шелла";
            this.checkBoxShell.UseVisualStyleBackColor = true;
            // 
            // checkBoxSpeedAnim
            // 
            this.checkBoxSpeedAnim.AutoSize = true;
            this.checkBoxSpeedAnim.Location = new System.Drawing.Point(6, 42);
            this.checkBoxSpeedAnim.Name = "checkBoxSpeedAnim";
            this.checkBoxSpeedAnim.Size = new System.Drawing.Size(186, 17);
            this.checkBoxSpeedAnim.TabIndex = 27;
            this.checkBoxSpeedAnim.Text = "Изменить скорасть аниимации";
            this.checkBoxSpeedAnim.UseVisualStyleBackColor = true;
            this.checkBoxSpeedAnim.CheckedChanged += new System.EventHandler(this.checkBoxSpeedAnim_CheckedChanged);
            // 
            // groupBoxSort
            // 
            this.groupBoxSort.Controls.Add(this.checkBoxBuble);
            this.groupBoxSort.Controls.Add(this.checkBoxShake);
            this.groupBoxSort.Controls.Add(this.checkBoxInsert);
            this.groupBoxSort.Controls.Add(this.checkBoxShell);
            this.groupBoxSort.Controls.Add(this.checkBoxMerge);
            this.groupBoxSort.Controls.Add(this.checkBoxSelect);
            this.groupBoxSort.Location = new System.Drawing.Point(12, 447);
            this.groupBoxSort.Name = "groupBoxSort";
            this.groupBoxSort.Size = new System.Drawing.Size(207, 162);
            this.groupBoxSort.TabIndex = 28;
            this.groupBoxSort.TabStop = false;
            this.groupBoxSort.Text = "Сортировки";
            // 
            // groupBoxSettings
            // 
            this.groupBoxSettings.Controls.Add(this.checkedListBoxGenerationArray);
            this.groupBoxSettings.Controls.Add(this.labelGenerationArray);
            this.groupBoxSettings.Controls.Add(this.labelCountElement);
            this.groupBoxSettings.Controls.Add(this.numericUpDownElement);
            this.groupBoxSettings.Controls.Add(this.checkBoxSpeedAnim);
            this.groupBoxSettings.Controls.Add(this.trackBarSpedAnim);
            this.groupBoxSettings.Location = new System.Drawing.Point(12, 272);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(213, 169);
            this.groupBoxSettings.TabIndex = 29;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = "Найстройки";
            // 
            // checkedListBoxGenerationArray
            // 
            this.checkedListBoxGenerationArray.BackColor = System.Drawing.SystemColors.Control;
            this.checkedListBoxGenerationArray.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.checkedListBoxGenerationArray.CheckOnClick = true;
            this.checkedListBoxGenerationArray.FormattingEnabled = true;
            this.checkedListBoxGenerationArray.Items.AddRange(new object[] {
            "Автоматическая",
            "Ручная",
            "Через файл"});
            this.checkedListBoxGenerationArray.Location = new System.Drawing.Point(6, 112);
            this.checkedListBoxGenerationArray.Name = "checkedListBoxGenerationArray";
            this.checkedListBoxGenerationArray.Size = new System.Drawing.Size(120, 45);
            this.checkedListBoxGenerationArray.TabIndex = 29;
            this.checkedListBoxGenerationArray.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // labelGenerationArray
            // 
            this.labelGenerationArray.AutoSize = true;
            this.labelGenerationArray.Location = new System.Drawing.Point(6, 96);
            this.labelGenerationArray.Name = "labelGenerationArray";
            this.labelGenerationArray.Size = new System.Drawing.Size(108, 13);
            this.labelGenerationArray.TabIndex = 28;
            this.labelGenerationArray.Text = "Генерация массива";
            // 
            // ComparemenuStrip
            // 
            this.ComparemenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileToolStripMenuItem,
            this.fAQToolStripMenuItem});
            this.ComparemenuStrip.Location = new System.Drawing.Point(0, 0);
            this.ComparemenuStrip.Name = "ComparemenuStrip";
            this.ComparemenuStrip.Size = new System.Drawing.Size(768, 24);
            this.ComparemenuStrip.TabIndex = 30;
            this.ComparemenuStrip.Text = "menuStrip1";
            // 
            // FileToolStripMenuItem
            // 
            this.FileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.открытьToolStripMenuItem,
            this.настройкиToolStripMenuItem,
            this.выходToolStripMenuItem});
            this.FileToolStripMenuItem.Name = "FileToolStripMenuItem";
            this.FileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.FileToolStripMenuItem.Text = "Файл";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.открытьToolStripMenuItem.Text = "Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            this.настройкиToolStripMenuItem.Click += new System.EventHandler(this.настройкиToolStripMenuItem_Click);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // fAQToolStripMenuItem
            // 
            this.fAQToolStripMenuItem.Name = "fAQToolStripMenuItem";
            this.fAQToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.fAQToolStripMenuItem.Text = "FAQ";
            this.fAQToolStripMenuItem.Click += new System.EventHandler(this.fAQToolStripMenuItem_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            // 
            // labelSourceArray
            // 
            this.labelSourceArray.AutoSize = true;
            this.labelSourceArray.Location = new System.Drawing.Point(12, 38);
            this.labelSourceArray.Name = "labelSourceArray";
            this.labelSourceArray.Size = new System.Drawing.Size(99, 13);
            this.labelSourceArray.TabIndex = 31;
            this.labelSourceArray.Text = "Исходный массив";
            // 
            // labelResultSort
            // 
            this.labelResultSort.AutoSize = true;
            this.labelResultSort.Location = new System.Drawing.Point(13, 140);
            this.labelResultSort.Name = "labelResultSort";
            this.labelResultSort.Size = new System.Drawing.Size(129, 13);
            this.labelResultSort.TabIndex = 32;
            this.labelResultSort.Text = "Результаты сортировки";
            // 
            // Compare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 746);
            this.Controls.Add(this.labelResultSort);
            this.Controls.Add(this.labelSourceArray);
            this.Controls.Add(this.groupBoxSettings);
            this.Controls.Add(this.groupBoxSort);
            this.Controls.Add(this.chart_shell);
            this.Controls.Add(this.chart_merge);
            this.Controls.Add(this.chart_select);
            this.Controls.Add(this.chart_insert);
            this.Controls.Add(this.textBoxResult);
            this.Controls.Add(this.chart_Shake);
            this.Controls.Add(this.chart_buble);
            this.Controls.Add(this.button_generation);
            this.Controls.Add(this.button_start_sort);
            this.Controls.Add(this.textBoxSourceArray);
            this.Controls.Add(this.ComparemenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.ComparemenuStrip;
            this.Name = "Compare";
            this.Text = "Сравнение сортировок";
            this.Load += new System.EventHandler(this.Compare_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart_buble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_Shake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownElement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpedAnim)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_insert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_select)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_merge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart_shell)).EndInit();
            this.groupBoxSort.ResumeLayout(false);
            this.groupBoxSort.PerformLayout();
            this.groupBoxSettings.ResumeLayout(false);
            this.groupBoxSettings.PerformLayout();
            this.ComparemenuStrip.ResumeLayout(false);
            this.ComparemenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_generation;
        private System.Windows.Forms.Button button_start_sort;
        private System.Windows.Forms.TextBox textBoxSourceArray;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_buble;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_Shake;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.CheckBox checkBoxBuble;
        private System.Windows.Forms.CheckBox checkBoxShake;
        private System.Windows.Forms.NumericUpDown numericUpDownElement;
        private System.Windows.Forms.Label labelCountElement;
        private System.Windows.Forms.TrackBar trackBarSpedAnim;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_insert;
        private System.Windows.Forms.CheckBox checkBoxInsert;
        private System.Windows.Forms.CheckBox checkBoxSelect;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_select;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_merge;
        private System.Windows.Forms.CheckBox checkBoxMerge;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_shell;
        private System.Windows.Forms.CheckBox checkBoxShell;
        private System.Windows.Forms.CheckBox checkBoxSpeedAnim;
        private System.Windows.Forms.GroupBox groupBoxSort;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.MenuStrip ComparemenuStrip;
        private System.Windows.Forms.ToolStripMenuItem FileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fAQToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label labelSourceArray;
        private System.Windows.Forms.Label labelResultSort;
        private System.Windows.Forms.Label labelGenerationArray;
        private System.Windows.Forms.CheckedListBox checkedListBoxGenerationArray;
    }
}