﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Compare : Form
    {
        Stopwatch stopwatch = new Stopwatch();//создание объекта для расчета времени алгоритмов сортировок
        public int[] array;//Главный массив данных, с помощью которого работают все сортировки
        public string ExportArray;
        int count;//количество элементов в массиве
        string ArrayFile;//Записывается в данную строку данные из файла
        string[] ResultArrayFile;//Массив строк, преобразовывает ArrayFile, в массив строк.
        string[] ResultArrayManual;//Массив строк, если выбран метод генерация вручную.

        public Compare()
        {

            InitializeComponent();
            
        }

        private void Compare_Load(object sender, EventArgs e)
        {
            //////////////////////////
            trackBarSpedAnim.Enabled = false;//Задает что Скорасть анимации изначально недоступен.
            button_start_sort.Enabled = false;//Задает что кнопка стар изначально недоступена.
            numericUpDownElement.Maximum = 10000;//Задает максимальное кол. элементов, для генерации автоматически.
            textBoxSourceArray.ReadOnly = true;//Задает что в textbox "исходный массиви" изначально писать нельзя
            textBoxResult.ReadOnly = true;//Задает что в textbox "результаты сортровки" изначально писать нельзя

            //////////////////////////////
            chart_buble.Series[0].ChartType = Properties.Settings.Default.TypeColums; //Задает что тип графика берется из конфига(можно поменять в настройках программы)
            chart_Shake.Series[0].ChartType = Properties.Settings.Default.TypeColums; //Задает что тип графика берется из конфига(можно поменять в настройках программы)
            chart_insert.Series[0].ChartType = Properties.Settings.Default.TypeColums; //Задает что тип графика берется из конфига(можно поменять в настройках программы)
            chart_select.Series[0].ChartType = Properties.Settings.Default.TypeColums; //Задает что тип графика берется из конфига(можно поменять в настройках программы)
            chart_merge.Series[0].ChartType = Properties.Settings.Default.TypeColums; //Задает что тип графика берется из конфига(можно поменять в настройках программы)
            chart_shell.Series[0].ChartType = Properties.Settings.Default.TypeColums; //Задает что тип графика берется из конфига(можно поменять в настройках программы)

            ///////////////////
            chart_buble.Series[0].Color = Properties.Settings.Default.ColumsNotSort; //Задает что цвет колонок берется из конфига(можно поменять в настройках программы)
            chart_buble.ChartAreas[0].AxisX.MajorGrid.Enabled = false;//Убирает сетку по x
            chart_buble.ChartAreas[0].AxisY.MajorGrid.Enabled = false; //Убирает сетку по y
            chart_buble.ChartAreas[0].AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по X
            chart_buble.ChartAreas[0].AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по Y
            //////////
            chart_Shake.Series[0].Color = Properties.Settings.Default.ColumsNotSort;//Задает что цвет колонок берется из конфига(можно поменять в настройках программы)
            chart_Shake.ChartAreas[0].AxisX.MajorGrid.Enabled = false;//Убирает сетку по x
            chart_Shake.ChartAreas[0].AxisY.MajorGrid.Enabled = false;//Убирает сетку по y
            chart_Shake.ChartAreas[0].AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по X
            chart_Shake.ChartAreas[0].AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по Y
            //////////
            chart_insert.Series[0].Color = Properties.Settings.Default.ColumsNotSort;//Задает что цвет колонок берется из конфига(можно поменять в настройках программы)
            chart_insert.ChartAreas[0].AxisX.MajorGrid.Enabled = false;//Убирает сетку по x
            chart_insert.ChartAreas[0].AxisY.MajorGrid.Enabled = false;//Убирает сетку по y
            chart_insert.ChartAreas[0].AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по X
            chart_insert.ChartAreas[0].AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по Y
            //////////
            chart_select.Series[0].Color = Properties.Settings.Default.ColumsNotSort;//Задает что цвет колонок берется из конфига(можно поменять в настройках программы)
            chart_select.ChartAreas[0].AxisX.MajorGrid.Enabled = false;//Убирает сетку по x
            chart_select.ChartAreas[0].AxisY.MajorGrid.Enabled = false;//Убирает сетку по y
            chart_select.ChartAreas[0].AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по X
            chart_select.ChartAreas[0].AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по Y
            //////////
            chart_merge.Series[0].Color = Properties.Settings.Default.ColumsNotSort;//Задает что цвет колонок берется из конфига(можно поменять в настройках программы)
            chart_merge.ChartAreas[0].AxisX.MajorGrid.Enabled = false;//Убирает сетку по x
            chart_merge.ChartAreas[0].AxisY.MajorGrid.Enabled = false;//Убирает сетку по y
            chart_merge.ChartAreas[0].AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по X
            chart_merge.ChartAreas[0].AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по Y
            //////////
            chart_shell.Series[0].Color = Properties.Settings.Default.ColumsNotSort;//Задает что цвет колонок берется из конфига(можно поменять в настройках программы)
            chart_shell.ChartAreas[0].AxisX.MajorGrid.Enabled = false;//Убирает сетку по x
            chart_shell.ChartAreas[0].AxisY.MajorGrid.Enabled = false;//Убирает сетку по y
            chart_shell.ChartAreas[0].AxisX.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по X
            chart_shell.ChartAreas[0].AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;//Убирает деление по Y

        }

        private void button__start_sort_Click(object sender, EventArgs e)
        {
            if (checkBoxBuble.Checked == true)//проверяет выставлен ли флаг на checkbox,чтобы запустить сортировку.
                BubleSort();//метод сортировки                                
            if (checkBoxShake.Checked == true)//проверяет выставлен ли флаг на checkbox,чтобы запустить сортировку.
                ShakerSort();//метод сортировки         
            if (checkBoxInsert.Checked == true)//проверяет выставлен ли флаг на checkbox,чтобы запустить сортировку.
                InsertSort();//метод сортировки         
            if (checkBoxSelect.Checked == true)//проверяет выставлен ли флаг на checkbox,чтобы запустить сортировку.
                SelectionSort();//метод сортировки         
            if (checkBoxMerge.Checked == true)//проверяет выставлен ли флаг на checkbox,чтобы запустить сортировку.
                Merge_start();//метод сортировки         
            if (checkBoxShell.Checked == true)//проверяет выставлен ли флаг на checkbox,чтобы запустить сортировку.
                ShellSort();//метод сортировки         
        }//Метод при нажатии на кнопку старт

        private void button__generation_Click(object sender, EventArgs e)//Метод при нажатии на кнопку генерации
        {
            ChartClear();//Вызывает метод очистки всех chart
            if(checkedListBoxGenerationArray.SelectedIndex == 0 || checkedListBoxGenerationArray.SelectedIndex == 2)//Если выбран метод генерации через файл или автоматически
                textBoxSourceArray.Clear();//Метод очистки textbox                                                  //то textbox "исходный массив" очищается.
            textBoxResult.Clear();//Метод очистки textbox

            Random rdm = new Random();//создание объекта рандом
            button_start_sort.Enabled = true;//Включает кнопку старт.

            try//Обработчик исключений
            {
                if (checkedListBoxGenerationArray.SelectedIndex == 0)//Если выбран Автоматический метод генерации массива
                {
                    count = (int)numericUpDownElement.Value;//Берет значения из элемента numericUpDown
                    array = new int[count];//инициализация массива 
                }
                if (checkedListBoxGenerationArray.SelectedIndex == 1)//Если выбран Вручную метод генерации массива
                {
                    string SourceArrayManual = textBoxSourceArray.Text.ToString();//Берем значения из textbox "исходный массив"
                    ResultArrayManual = SourceArrayManual.Trim().Split(' ');//Переводим строку в массив строк.
                    array = new int[ResultArrayManual.Length];//Определяет размер массива
                    count = ResultArrayManual.Length;//Определяет количество элементов для цикла
                }
                if (checkedListBoxGenerationArray.SelectedIndex == 2)//Если выбран Через файл метод генерации массива
                {
                    ResultArrayFile = ArrayFile.Trim().Split(' ');//Переводим строку в массив строк.
                    array = new int[ResultArrayFile.Length];//Определяет размер массива
                    count = ResultArrayFile.Length;//Определяет количество элементов для цикла
                }
            }
            catch(NullReferenceException)//Обработчик исключений
            {
                MessageBox.Show("Ошибка!Выберите  файл для заполнения массива.");//Такое сообщение появится, если произойдет ошибка.
            }
            

            for (int i = 0; i < count; i++)//цикл
            {  
                try//обработчик исключений 
                {
                    if (checkedListBoxGenerationArray.SelectedIndex == 0)//Если выбран Автоматический метод генерации массива
                        array[i] = rdm.Next(1, 100);//заполнение массива рандомными цифрами
                    else if (checkedListBoxGenerationArray.SelectedIndex == 1)//Если выбран Вручную метод генерации массива
                        array[i] = Convert.ToInt32(ResultArrayManual[i]);
                    else if (checkedListBoxGenerationArray.SelectedIndex == 2)//Если выбран Через файл метод генерации массива
                        array[i] = Convert.ToInt32(ResultArrayFile[i]);
                    
                }
                catch (FormatException)//обработчик исключений 
                {
                    MessageBox.Show("Ошибка!Заполните массив.");//Такое сообщение появится, если произойдет ошибка.
                    break;//выходит из цикла
                } 
                
                if (checkBoxBuble.Checked == true)//проверяет выставлен ли флаг на checkbox,отрисовать заполненый массив.
                    chart_buble.Series[0].Points.AddY(array[i]);//отрисовка массива, в качестве переменной берется i-ый элемент массива
                if (checkBoxShake.Checked == true)//проверяет выставлен ли флаг на checkbox,отрисовать заполненый массив.
                    chart_Shake.Series[0].Points.AddY(array[i]);//отрисовка массива, в качестве переменной берется i-ый элемент массива
                if (checkBoxInsert.Checked == true)//проверяет выставлен ли флаг на checkbox,отрисовать заполненый массив.
                    chart_insert.Series[0].Points.AddY(array[i]);//отрисовка массива, в качестве переменной берется i-ый элемент массива
                if (checkBoxSelect.Checked == true)//проверяет выставлен ли флаг на checkbox,отрисовать заполненый массив.
                    chart_select.Series[0].Points.AddY(array[i]);//отрисовка массива, в качестве переменной берется i-ый элемент массива
                if (checkBoxMerge.Checked == true)//проверяет выставлен ли флаг на checkbox,отрисовать заполненый массив.
                    chart_merge.Series[0].Points.AddY(array[i]);//отрисовка массива, в качестве переменной берется i-ый элемент массива
                if (checkBoxShell.Checked == true)//проверяет выставлен ли флаг на checkbox,отрисовать заполненый массив.
                    chart_shell.Series[0].Points.AddY(array[i]);//отрисовка массива, в качестве переменной берется i-ый элемент массива

                if (checkedListBoxGenerationArray.SelectedIndex == 0 || checkedListBoxGenerationArray.SelectedIndex == 2)//Если выбран Автоматический метод или Через файл, то выведит их значения в  textbox "исходный массив"
                textBoxSourceArray.Text = textBoxSourceArray.Text + "    " + Convert.ToString(array[i]);//заполнение textbox "исходный массив"
            }
        }

        private void checkBoxSpeedAnim_CheckedChanged(object sender, EventArgs e)//Метод при нажатии checkbox "Изменить скорасть аниимации"
        {
            if (checkBoxSpeedAnim.Checked == false)//Если checkbox "Изменить скорасть аниимации" false
                trackBarSpedAnim.Enabled = false;//То trackBar "Скорасть анимации" недоступна

            else if (checkBoxSpeedAnim.Checked == true)//Если checkbox "Изменить скорасть аниимации" True
                trackBarSpedAnim.Enabled = true;//То trackBar "Скорасть анимации" доступна
        }

        private void SetColorGray(int i,int sort)// метод покраски в серый цвет в параметрах 
                                                // передается индекс и номер сортировка
        {
            if(sort==0)//проверяет какая сортировка
            {
                chart_buble.Series[0].Points[i].Color = Properties.Settings.Default.SortedColums; // покраска в серый
                chart_buble.Update();
            }
                
            if(sort==1)
            {
                chart_Shake.Series[0].Points[i].Color = Properties.Settings.Default.SortedColums;
                chart_Shake.Update();
            }

            if (sort == 2)
            {
                chart_insert.Series[0].Points[i].Color = Properties.Settings.Default.SortedColums;
                chart_insert.Update();
            }

            if (sort == 3)
            {
                chart_select.Series[0].Points[i].Color = Properties.Settings.Default.SortedColums;
                chart_select.Update();
            }

            if (sort == 4)
            {
                chart_shell.Series[0].Points[i].Color = Properties.Settings.Default.SortedColums;
                chart_shell.Update();
            }

            if (sort == 5)
            {
                chart_merge.Series[0].Points[i].Color = Properties.Settings.Default.SortedColums;
                chart_merge.Update();
            }
        }

        private void SetColorGold(int i, int j,int sort)
        {
            if(sort==0)
            {
                chart_buble.Series[0].Points[i].Color = Properties.Settings.Default.ColumsNotSort;
                chart_buble.Series[0].Points[j].Color = Properties.Settings.Default.ColumsNotSort;
                chart_buble.Update();
            }

            if(sort==1)
            {
                if (chart_Shake.Series[0].Points[i].Color == Properties.Settings.Default.SortedColums || chart_Shake.Series[0].Points[j].Color == Properties.Settings.Default.SortedColums) { }
                else
                {
                    chart_Shake.Series[0].Points[i].Color = Properties.Settings.Default.ColumsNotSort;
                    chart_Shake.Series[0].Points[j].Color = Properties.Settings.Default.ColumsNotSort;
                    chart_Shake.Update();
                }
            }

            if (sort == 2)
            {
                if (chart_insert.Series[0].Points[i].Color == Properties.Settings.Default.SortedColums || chart_insert.Series[0].Points[j].Color == Properties.Settings.Default.SortedColums) { }
                else
                {
                    chart_insert.Series[0].Points[i].Color = Properties.Settings.Default.ColumsNotSort;
                    chart_insert.Series[0].Points[j].Color = Properties.Settings.Default.ColumsNotSort;
                    chart_insert.Update();
                }
            }

            if (sort == 3)
            {
                    chart_select.Series[0].Points[i].Color = Properties.Settings.Default.ColumsNotSort;
                if (chart_select.Series[0].Points[j].Color != Color.Red)
                    chart_select.Series[0].Points[j].Color = Properties.Settings.Default.ColumsNotSort;
                chart_select.Update();
            }

            if (sort == 4)
            {
                if (chart_shell.Series[0].Points[i].Color == Properties.Settings.Default.SortedColums) { }
                else
                {
                    chart_shell.Series[0].Points[i].Color = Properties.Settings.Default.ColumsNotSort;
                    chart_shell.Series[0].Points[j].Color = Properties.Settings.Default.ColumsNotSort;
                    chart_shell.Update();
                }
            }

            if (sort == 5)
            {
                chart_merge.Series[0].Points[i].Color = Properties.Settings.Default.ColumsNotSort;
                chart_merge.Series[0].Points[j].Color = Properties.Settings.Default.ColumsNotSort;
                chart_merge.Update();
            }

        }

        private void SetColorGreen(int i, int j,int sort)
        {
            if (sort == 0)
            {
                chart_buble.Series[0].Points[i].Color = Properties.Settings.Default.ColumsIfSort;
                chart_buble.Series[0].Points[j].Color = Properties.Settings.Default.ColumsIfSort;
                chart_buble.Update();
            }

            if (sort == 1)
            {
                chart_Shake.Series[0].Points[i].Color = Properties.Settings.Default.ColumsIfSort;
                chart_Shake.Series[0].Points[j].Color = Properties.Settings.Default.ColumsIfSort;
                chart_Shake.Update();
            }

            if (sort == 2)
            {
                if (chart_insert.Series[0].Points[i].Color == Properties.Settings.Default.SortedColums) { }
                else
                {
                    chart_insert.Series[0].Points[i].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_insert.Series[0].Points[j].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_insert.Update();
                }
            }
            if (sort == 3)
            {
                if(chart_select.Series[0].Points[i].Color!=Color.Red)
                chart_select.Series[0].Points[i].Color = Properties.Settings.Default.ColumsIfSort;
                if (chart_select.Series[0].Points[j].Color != Color.Red)
                    chart_select.Series[0].Points[j].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_select.Update();
            }

            if (sort == 4)
            {
                if (chart_shell.Series[0].Points[i].Color == Properties.Settings.Default.SortedColums) { }
                else
                {
                    chart_shell.Series[0].Points[i].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_shell.Series[0].Points[j].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_shell.Update();
                }
            }
            if (sort == 5)
            {
                {
                    chart_merge.Series[0].Points[i].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_merge.Series[0].Points[j].Color = Properties.Settings.Default.ColumsIfSort;
                    chart_merge.Update();
                }
            }
        }

        private void SetColorRed(int i,int sort)
        {
            if (sort == 0)
            {
                chart_buble.Series[0].Points[i].Color = Color.Red;
                chart_buble.Update();
            }

            if (sort == 1)
            {
                chart_Shake.Series[0].Points[i].Color = Color.Red;
                chart_Shake.Update();
            }

            if (sort == 2)
            {
                chart_insert.Series[0].Points[i].Color = Color.Red;
                chart_insert.Update();
            }

            if (sort == 3)
            {
                chart_select.Series[0].Points[i].Color = Color.Red;
                chart_select.Update();
            }

            if (sort == 4)
            {
                chart_shell.Series[0].Points[i].Color = Color.Red;
                chart_shell.Update();
            }

            if (sort == 5)
            {
                chart_merge.Series[0].Points[i].Color = Color.Red;
                chart_merge.Update();
            }
        }

        private void SwapColumn(int[] array,int i, int j,int sort)
        {
            if(sort==0)
            {
                chart_buble.Series[0].Points.InsertY(i, array[i]);
                chart_buble.Series[0].Points.RemoveAt(i + 1);
                chart_buble.Series[0].Points.InsertY(j, array[j]);
                chart_buble.Series[0].Points.RemoveAt(j + 1);
                chart_buble.Update();
            }

            if(sort==1)
            {
                chart_Shake.Series[0].Points.InsertY(i, array[i]);
                chart_Shake.Series[0].Points.RemoveAt(i + 1);
                chart_Shake.Series[0].Points.InsertY(j, array[j]);
                chart_Shake.Series[0].Points.RemoveAt(j + 1);
                chart_Shake.Update();
            }

            if (sort == 2)
            {
                chart_insert.Series[0].Points.InsertY(i, array[i]);
                chart_insert.Series[0].Points.RemoveAt(i + 1);
                chart_insert.Series[0].Points.InsertY(j, array[j]);
                chart_insert.Series[0].Points.RemoveAt(j + 1);
                chart_insert.Update();
            }

            if (sort == 3)
            {
                chart_select.Series[0].Points.InsertY(i, array[i]);
                chart_select.Series[0].Points.RemoveAt(i + 1);
                chart_select.Series[0].Points.InsertY(j, array[j]);
                chart_select.Series[0].Points.RemoveAt(j + 1);
                chart_select.Update();
            }

            if (sort == 4)
            {
                chart_shell.Series[0].Points.InsertY(i, array[i]);
                chart_shell.Series[0].Points[i].Color = chart_shell.Series[0].Points[i + 1].Color;
                chart_shell.Series[0].Points.RemoveAt(i + 1);
                chart_shell.Series[0].Points.InsertY(j, array[j]);
                chart_shell.Series[0].Points[j].Color = chart_shell.Series[0].Points[j + 1].Color;
                chart_shell.Series[0].Points.RemoveAt(j + 1);
                chart_shell.Update();
            }
            if (sort == 5)
            {
                chart_merge.Series[0].Points.InsertY(i, array[i]);
                chart_merge.Series[0].Points.RemoveAt(i + 1);
                chart_merge.Series[0].Points.InsertY(j, array[j]);
                chart_merge.Series[0].Points.RemoveAt(j + 1);
                chart_merge.Update();
            }
        }

        private void swap<T>(ref T a,ref T b)
        {
            T c = a;
            a = b;
            b = c;
        }

        private void ChartClear()
        {
            if (checkBoxBuble.Checked == true)
                foreach (var series in chart_buble.Series)
                {
                    series.Points.Clear();
                }
            else chart_buble.Series[0].Points.Clear();

            if (checkBoxShake.Checked == true)
                foreach (var series in chart_Shake.Series)
            {
                series.Points.Clear();
            }
            else chart_Shake.Series[0].Points.Clear();

            if (checkBoxInsert.Checked == true)
                foreach (var series in chart_insert.Series)
                {
                    series.Points.Clear();
                }
            else chart_insert.Series[0].Points.Clear();

            if (checkBoxSelect.Checked == true)
                foreach (var series in chart_select.Series)
                {
                    series.Points.Clear();
                }
            else chart_select.Series[0].Points.Clear();

            if (checkBoxShell.Checked == true)
                foreach (var series in chart_shell.Series)
                {
                    series.Points.Clear();
                }
            else chart_shell.Series[0].Points.Clear();

            if (checkBoxMerge.Checked == true)
                foreach (var series in chart_merge.Series)
                {
                    series.Points.Clear();
                }
            else chart_merge.Series[0].Points.Clear();
        }

        private void showresult(string result,int sort)
        {
            switch(sort)
            {
                case 0:
                    textBoxResult.Text = textBoxResult.Text + "Сортировка пузырьком: " + result + "\n";
                    break;
                case 1:
                    textBoxResult.Text = textBoxResult.Text + "Шейкерная сортировка: " + result + "\n";
                    break;
                case 2:
                    textBoxResult.Text = textBoxResult.Text + "Сортировка вставками: " + result + "\n";
                    break;
                case 3:
                    textBoxResult.Text = textBoxResult.Text + "Сортировка выбором: " + result + "\n";
                    break;
                case 4:
                    textBoxResult.Text = textBoxResult.Text + "Сортировка Шелла: " + result + "\n";
                    break;
                case 5:
                    textBoxResult.Text = textBoxResult.Text + "Сортировка Слиянием: " + result + "\n";
                    break;
            }
                
        }

        private void BubleSort()
        {
            try
            {

                int[] array = new int[count];
                for (int i = 0; i < this.array.Length; i++)
                    array[i] = this.array[i];

                stopwatch.Start();
  
                for (int i = 0; i < array.Length; i++)
                {
                    for (int j = i+1; j < array.Length;j++)
                    {
                        SetColorGreen(i, j, 0);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        if (array[i] > array[j])
                        {
                            SetColorRed(j, 0);
                            if (checkBoxSpeedAnim.Checked == true)
                                Thread.Sleep(trackBarSpedAnim.Value);

                            swap(ref array[i], ref array[j]);
                            SwapColumn(array, i, j, 0);
                        }
                        SetColorGold(i, j, 0);
                    }
                }

                for (int i = 0; i < array.Length; i++)
                {
                    SetColorGray(i, 0);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);
                    else
                        Thread.Sleep(5);
                }

                stopwatch.Stop();
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}", stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
                showresult(elapsedTime, 0);
                stopwatch.Reset();
            }
         
            catch(NullReferenceException)
            {
                MessageBox.Show("Ошибка!Выберите  тип генерации и сгенирируйте массив");
            }
        }
           
        private void ShakerSort()
        {
            try
            {
                int[] array = new int[count];
                for (int i = 0; i < this.array.Length; i++)
                    array[i] = this.array[i];

                int left = 0, right = array.Length - 1;
                stopwatch.Start();

                while (left <= right)
                {
                    for (int i = left; i < right; i++)
                    {
                        SetColorGreen(i, i + 1, 1);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        if (array[i] > array[i + 1])
                        {
                            SetColorRed(i + 1, 1);
                            if (checkBoxSpeedAnim.Checked == true)
                                Thread.Sleep(trackBarSpedAnim.Value);

                            swap(ref array[i], ref array[i + 1]);
                            SwapColumn(array, i, i + 1, 1);
                        }

                        SetColorGold(i, i + 1, 1);
                    }
                    right--;

                    for (int i = right; i > left; i--)
                    {
                        SetColorGreen(i - 1, i, 1);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        if (array[i - 1] > array[i])
                        {
                            SetColorRed(i, 1);
                            if (checkBoxSpeedAnim.Checked == true)
                                Thread.Sleep(trackBarSpedAnim.Value);

                            swap(ref array[i - 1], ref array[i]);
                            SwapColumn(array, i - 1, i, 1);
                        }

                        SetColorGold(i-1, i, 1);
                    }
                    left++;
                }

                for (int i = 0; i < array.Length; i++)
                {
                    SetColorGray(i, 1);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);
                    else
                        Thread.Sleep(5);
                }

                stopwatch.Stop();
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}", stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
                showresult(elapsedTime, 1);
                stopwatch.Reset();
                
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Ошибка!Выберите  тип генерации и сгенирируйте массив");
            }
        }

        private void InsertSort()
        {
            try
            {
                int[] array = new int[count];
                for (int i = 0; i < this.array.Length; i++)
                    array[i] = this.array[i];

                stopwatch.Start();
                for (int i = 0; i < array.Length; i++)
                {
                    int cur = array[i];
                    int j = i;

                    while (j > 0 && cur < array[j - 1])
                    {
                        SetColorGreen(j, j - 1, 2);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        SetColorRed(j, 2);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        swap(ref array[j], ref array[j - 1]);
                        SwapColumn(array, j, j - 1, 2);

                        SetColorGold(j, j - 1, 2);
                        j--;

                    }
                }

                for(int i=0;i<array.Length;i++)
                {
                    SetColorGray(i, 2);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);
                    else
                        Thread.Sleep(5);
                }

                stopwatch.Stop();
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}", stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
                showresult(elapsedTime, 2);
                stopwatch.Reset();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Ошибка!Выберите  тип генерации и сгенирируйте массив");
            }
          
        }

        private void SelectionSort()
        {
            try
            {
                int[] array = new int[count];
                for (int i = 0; i < this.array.Length; i++)
                    array[i] = this.array[i];

                int min;
                int clear_min=0;
                int length = array.Length;

                stopwatch.Start();
                for (int i = 0; i < length - 1; i++)
                {
                    min = i;
                    for (int j = i + 1; j < length; j++)
                    {
                        SetColorGreen(i, j, 3);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        if (array[j] < array[min])
                        {
                            SetColorRed(j, 3);
                            if (checkBoxSpeedAnim.Checked == true)
                                Thread.Sleep(trackBarSpedAnim.Value);
                            clear_min = min;
                            min = j;
                        }
                        SetColorGold(clear_min, j, 3);
                        SetColorGold(i, j, 3);

                    }

                    if (min != i)
                    {
                        swap(ref array[i], ref array[min]);
                        SwapColumn(array, i, min, 3);
                        SetColorGold(i, min, 3);
                    }
                  
                }

                for (int i = 0; i < array.Length; i++)
                {
                    SetColorGray(i, 3);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);
                    else
                        Thread.Sleep(5);
                }

                stopwatch.Stop();
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}", stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
                showresult(elapsedTime, 3);
                stopwatch.Reset();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Ошибка!Выберите  тип генерации и сгенирируйте массив");
            }
           
        }

        private void ShellSort()
        {
            try
            {
                int[] array = new int[count];
                for (int i = 0; i < this.array.Length; i++)
                    array[i] = this.array[i];

                int j;
                int step = array.Length / 2;
                stopwatch.Start();
                while (step > 0)
                {
                    for (int i = 0; i < (array.Length - step); i++)
                    {
                        j = i;
                        while ((j >= 0) && (array[j] > array[j + step]))
                        {

                            SetColorGreen(j, j + step, 4);
                            if (checkBoxSpeedAnim.Checked == true)
                                Thread.Sleep(trackBarSpedAnim.Value);

                            SetColorRed(j + step,4);
                            if (checkBoxSpeedAnim.Checked == true)
                                Thread.Sleep(trackBarSpedAnim.Value);

                            swap(ref array[j], ref array[j + step]);
                            SwapColumn(array, j, j + step, 4);
                            SetColorGold(j, j + step, 4);

                            j -= step;

                        }
                    }
                    step = step / 2;
                }

                for (int i = 0; i < array.Length; i++)
                {
                    SetColorGray(i, 4);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);
                    else
                        Thread.Sleep(5);
                }


                stopwatch.Stop();
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}", stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
                showresult(elapsedTime, 4);
                stopwatch.Reset();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Ошибка!Выберите  тип генерации и сгенирируйте массив");
            }
           
        }

        private void Merge_start()
        {
            try
            {
                int[] array = new int[count];
                for (int i = 0; i < this.array.Length; i++)
                    array[i] = this.array[i];

                stopwatch.Start();
                int[] ar = Merge_Sort(array);

                for (int i = 0; i < array.Length; i++)
                {
                    SetColorGray(i, 5);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);
                    else
                        Thread.Sleep(5);
                }

                stopwatch.Stop();
                string elapsedTime = string.Format("{0:00}:{1:00}:{2:00}", stopwatch.Elapsed.Minutes, stopwatch.Elapsed.Seconds, stopwatch.Elapsed.Milliseconds / 10);
                showresult(elapsedTime, 5);
                stopwatch.Reset();
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Ошибка!Выберите  тип генерации и сгенирируйте массив");
            }
            
        }

        private Int32[] Merge_Sort(Int32[] array)
        {
            if (array.Length == 1)
                return array;
            Int32 mid_point = array.Length / 2;
            return Merge(Merge_Sort(array.Take(mid_point).ToArray()), Merge_Sort(array.Skip(mid_point).ToArray()));
        }

        private Int32[] Merge(Int32[] left, Int32[] right)
        {

            Int32 a = 0, b = 0;
            Int32[] merged = new int[left.Length + right.Length];
            for (Int32 i = 0; i < left.Length + right.Length; i++)
            {
                if (b < right.Length && a < left.Length)
                {
                    SetColorGreen(a, b, 5);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);

                    if (left[a] > right[b])
                    {
                        SetColorRed(b, 5);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                       // merged[i] = right[b++];
                        swap(ref merged[i], ref right[b++]);
                        SwapColumn(merged, i, b,5);

                        SetColorGold(a, b-1, 5);
                    }

                    else //if int go for
                    {
                        SetColorRed(a, 5);
                        if (checkBoxSpeedAnim.Checked == true)
                            Thread.Sleep(trackBarSpedAnim.Value);

                        swap(ref merged[i], ref left[a++]);
                        SwapColumn(merged, i, a, 5);

                        SetColorGold(a-1, b, 5);
                    }
                }   
                else
                  if (b < right.Length)
                {
                    SetColorRed(b, 5);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);

                    swap(ref merged[i], ref right[b++]);
                    SwapColumn(merged, i, b, 5);

                    SetColorGold(a, b-1, 5);
                }
                else
                {
                    SetColorRed(a, 5);
                    if (checkBoxSpeedAnim.Checked == true)
                        Thread.Sleep(trackBarSpedAnim.Value);

                    swap(ref merged[i], ref left[a++]);
                    SwapColumn(merged, i, a, 5);

                    SetColorGold(a - 1, b, 5);
                }
            }
                return merged;
        }

        private void настройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Compare_Settings compare_Settings = new Compare_Settings();
            compare_Settings.Show();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                ArrayFile = File.ReadAllText(openFileDialog.FileName);
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (checkedListBoxGenerationArray.SelectedIndex)
            {
                case 0:
                    checkedListBoxGenerationArray.SetItemChecked(1,false);
                    checkedListBoxGenerationArray.SetItemChecked(2, false);
                    numericUpDownElement.Enabled = true;
                    textBoxSourceArray.ReadOnly = true;
                    break;
                case 1:
                    checkedListBoxGenerationArray.SetItemChecked(0, false);
                    checkedListBoxGenerationArray.SetItemChecked(2, false);
                    numericUpDownElement.Enabled = false;
                    textBoxSourceArray.ReadOnly = false;
                    break;
                case 2:
                    checkedListBoxGenerationArray.SetItemChecked(1, false);
                    checkedListBoxGenerationArray.SetItemChecked(0, false);
                    numericUpDownElement.Enabled = false;
                    textBoxSourceArray.ReadOnly = false;
                    break;

            }
        }

        private void fAQToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FAQ_Compare Form_FAQ_Compare = new FAQ_Compare();
            Form_FAQ_Compare.Show();
        }

        private void textBoxSourceArray_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8 && number != ' ') // цифры, клавиша BackSpace и запятая
            {
                e.Handled = true;
            }
        }

    }
}
