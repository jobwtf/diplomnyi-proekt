﻿using System;
using System.Drawing;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Compare_Settings : Form
    {
        public enum SeriesChartType { };

        public Compare_Settings()
        {
            InitializeComponent();
        }

        private void Compare_Settings_Load(object sender, EventArgs e)
        {
            buttonAppy.Enabled = false;
            pictureBoxNotSortColums.BackColor = Properties.Settings.Default.ColumsNotSort;
            pictureBoxIfColums.BackColor = Properties.Settings.Default.ColumsIfSort;
            pictureBoxSortedColums.BackColor = Properties.Settings.Default.SortedColums;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reset();
            Close();
        }

        private void buttonNotSortColums_Click(object sender, EventArgs e)
        {
            if (colorDialogColumsNotSort.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.ColumsNotSort = colorDialogColumsNotSort.Color;
                pictureBoxNotSortColums.BackColor=colorDialogColumsNotSort.Color;
                buttonAppy.Enabled = true;
            }
                
        }

        private void buttonIfColums_Click(object sender, EventArgs e)
        {
            if (colorDialogIfSort.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.ColumsIfSort = colorDialogIfSort.Color;
                pictureBoxIfColums.BackColor = colorDialogIfSort.Color;
                buttonAppy.Enabled = true;
            }
                

        }

        private void buttonSortedColums_Click(object sender, EventArgs e)
        {
            if (colorDialogSortedColums.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.SortedColums = colorDialogSortedColums.Color;
                pictureBoxSortedColums.BackColor = colorDialogSortedColums.Color;
                buttonAppy.Enabled = true;
            }
                
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            MessageBox.Show("При изменения настроек, требуется перезапустить окно сортировки!");
            Close();
        }

        private void buttonAppy_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            buttonAppy.Enabled = false;
        }

        private void buttonDefault_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.ColumsNotSort = Color.Gold;
            Properties.Settings.Default.ColumsIfSort = Color.Green;
            Properties.Settings.Default.SortedColums = Color.Gray;
            pictureBoxNotSortColums.BackColor = Color.Gold;
            pictureBoxIfColums.BackColor = Color.Green;
            pictureBoxSortedColums.BackColor = Color.Gray;
            Properties.Settings.Default.Save();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch(comboBoxTypeChart.SelectedItem)
            {
                case "Point":
                    {
                        Properties.Settings.Default.TypeColums = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
                        Properties.Settings.Default.ComboBoxTypeChartText = "Point";
                        buttonAppy.Enabled = true;
                    }
                    break;

                case "Area":
                    {
                        Properties.Settings.Default.TypeColums = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
                        Properties.Settings.Default.ComboBoxTypeChartText = "Area";
                        buttonAppy.Enabled = true;
                    }
                    break;

                case "Column":
                    {
                        Properties.Settings.Default.TypeColums = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;
                        Properties.Settings.Default.ComboBoxTypeChartText = "Column";
                        buttonAppy.Enabled = true;
                    }
                    break;

            }
        }
    }
}
