﻿namespace WindowsFormsApp1
{
    partial class FAQ_Compare
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FAQ_Compare));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBoxFAQ = new System.Windows.Forms.PictureBox();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.textBoxLegend = new System.Windows.Forms.TextBox();
            this.textBoxFAQ = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFAQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 61.75F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.25F));
            this.tableLayoutPanel.Controls.Add(this.pictureBoxFAQ, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.splitContainer, 1, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 1;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(800, 450);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // pictureBoxFAQ
            // 
            this.pictureBoxFAQ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxFAQ.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFAQ.Image")));
            this.pictureBoxFAQ.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxFAQ.Name = "pictureBoxFAQ";
            this.pictureBoxFAQ.Size = new System.Drawing.Size(488, 444);
            this.pictureBoxFAQ.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFAQ.TabIndex = 0;
            this.pictureBoxFAQ.TabStop = false;
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(497, 3);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.textBoxLegend);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.textBoxFAQ);
            this.splitContainer.Size = new System.Drawing.Size(300, 444);
            this.splitContainer.SplitterDistance = 189;
            this.splitContainer.TabIndex = 1;
            // 
            // textBoxLegend
            // 
            this.textBoxLegend.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLegend.Location = new System.Drawing.Point(3, 3);
            this.textBoxLegend.Multiline = true;
            this.textBoxLegend.Name = "textBoxLegend";
            this.textBoxLegend.ReadOnly = true;
            this.textBoxLegend.Size = new System.Drawing.Size(294, 183);
            this.textBoxLegend.TabIndex = 0;
            this.textBoxLegend.Text = resources.GetString("textBoxLegend.Text");
            // 
            // textBoxFAQ
            // 
            this.textBoxFAQ.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFAQ.Location = new System.Drawing.Point(3, 3);
            this.textBoxFAQ.Multiline = true;
            this.textBoxFAQ.Name = "textBoxFAQ";
            this.textBoxFAQ.ReadOnly = true;
            this.textBoxFAQ.Size = new System.Drawing.Size(294, 338);
            this.textBoxFAQ.TabIndex = 2;
            this.textBoxFAQ.Text = resources.GetString("textBoxFAQ.Text");
            // 
            // FAQ_Compare
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FAQ_Compare";
            this.Text = "FAQ";
            this.tableLayoutPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFAQ)).EndInit();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel1.PerformLayout();
            this.splitContainer.Panel2.ResumeLayout(false);
            this.splitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.PictureBox pictureBoxFAQ;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.TextBox textBoxLegend;
        private System.Windows.Forms.TextBox textBoxFAQ;
    }
}