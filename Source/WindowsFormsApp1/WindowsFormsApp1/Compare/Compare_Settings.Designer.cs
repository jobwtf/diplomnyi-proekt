﻿namespace WindowsFormsApp1
{
    partial class Compare_Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Compare_Settings));
            this.buttonNotSortColums = new System.Windows.Forms.Button();
            this.groupBoxColor = new System.Windows.Forms.GroupBox();
            this.pictureBoxSortedColums = new System.Windows.Forms.PictureBox();
            this.pictureBoxIfColums = new System.Windows.Forms.PictureBox();
            this.pictureBoxNotSortColums = new System.Windows.Forms.PictureBox();
            this.labelSortedColums = new System.Windows.Forms.Label();
            this.buttonSortedColums = new System.Windows.Forms.Button();
            this.labelIfColums = new System.Windows.Forms.Label();
            this.buttonIfColums = new System.Windows.Forms.Button();
            this.labelNotSortColums = new System.Windows.Forms.Label();
            this.buttonAppy = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.colorDialogColumsNotSort = new System.Windows.Forms.ColorDialog();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.groupBoxTypeChart = new System.Windows.Forms.GroupBox();
            this.comboBoxTypeChart = new System.Windows.Forms.ComboBox();
            this.colorDialogIfSort = new System.Windows.Forms.ColorDialog();
            this.colorDialogSortedColums = new System.Windows.Forms.ColorDialog();
            this.groupBoxColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSortedColums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIfColums)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNotSortColums)).BeginInit();
            this.groupBoxTypeChart.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonNotSortColums
            // 
            resources.ApplyResources(this.buttonNotSortColums, "buttonNotSortColums");
            this.buttonNotSortColums.Name = "buttonNotSortColums";
            this.buttonNotSortColums.UseVisualStyleBackColor = true;
            this.buttonNotSortColums.Click += new System.EventHandler(this.buttonNotSortColums_Click);
            // 
            // groupBoxColor
            // 
            resources.ApplyResources(this.groupBoxColor, "groupBoxColor");
            this.groupBoxColor.Controls.Add(this.pictureBoxSortedColums);
            this.groupBoxColor.Controls.Add(this.pictureBoxIfColums);
            this.groupBoxColor.Controls.Add(this.pictureBoxNotSortColums);
            this.groupBoxColor.Controls.Add(this.labelSortedColums);
            this.groupBoxColor.Controls.Add(this.buttonSortedColums);
            this.groupBoxColor.Controls.Add(this.labelIfColums);
            this.groupBoxColor.Controls.Add(this.buttonIfColums);
            this.groupBoxColor.Controls.Add(this.labelNotSortColums);
            this.groupBoxColor.Controls.Add(this.buttonNotSortColums);
            this.groupBoxColor.Name = "groupBoxColor";
            this.groupBoxColor.TabStop = false;
            // 
            // pictureBoxSortedColums
            // 
            resources.ApplyResources(this.pictureBoxSortedColums, "pictureBoxSortedColums");
            this.pictureBoxSortedColums.Name = "pictureBoxSortedColums";
            this.pictureBoxSortedColums.TabStop = false;
            // 
            // pictureBoxIfColums
            // 
            resources.ApplyResources(this.pictureBoxIfColums, "pictureBoxIfColums");
            this.pictureBoxIfColums.Name = "pictureBoxIfColums";
            this.pictureBoxIfColums.TabStop = false;
            // 
            // pictureBoxNotSortColums
            // 
            resources.ApplyResources(this.pictureBoxNotSortColums, "pictureBoxNotSortColums");
            this.pictureBoxNotSortColums.Name = "pictureBoxNotSortColums";
            this.pictureBoxNotSortColums.TabStop = false;
            // 
            // labelSortedColums
            // 
            resources.ApplyResources(this.labelSortedColums, "labelSortedColums");
            this.labelSortedColums.Name = "labelSortedColums";
            // 
            // buttonSortedColums
            // 
            resources.ApplyResources(this.buttonSortedColums, "buttonSortedColums");
            this.buttonSortedColums.Name = "buttonSortedColums";
            this.buttonSortedColums.UseVisualStyleBackColor = true;
            this.buttonSortedColums.Click += new System.EventHandler(this.buttonSortedColums_Click);
            // 
            // labelIfColums
            // 
            resources.ApplyResources(this.labelIfColums, "labelIfColums");
            this.labelIfColums.Name = "labelIfColums";
            // 
            // buttonIfColums
            // 
            resources.ApplyResources(this.buttonIfColums, "buttonIfColums");
            this.buttonIfColums.Name = "buttonIfColums";
            this.buttonIfColums.UseVisualStyleBackColor = true;
            this.buttonIfColums.Click += new System.EventHandler(this.buttonIfColums_Click);
            // 
            // labelNotSortColums
            // 
            resources.ApplyResources(this.labelNotSortColums, "labelNotSortColums");
            this.labelNotSortColums.Name = "labelNotSortColums";
            // 
            // buttonAppy
            // 
            resources.ApplyResources(this.buttonAppy, "buttonAppy");
            this.buttonAppy.Name = "buttonAppy";
            this.buttonAppy.UseVisualStyleBackColor = true;
            this.buttonAppy.Click += new System.EventHandler(this.buttonAppy_Click);
            // 
            // buttonCancel
            // 
            resources.ApplyResources(this.buttonCancel, "buttonCancel");
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // colorDialogColumsNotSort
            // 
            this.colorDialogColumsNotSort.Color = System.Drawing.Color.Gold;
            // 
            // buttonOk
            // 
            resources.ApplyResources(this.buttonOk, "buttonOk");
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonDefault
            // 
            resources.ApplyResources(this.buttonDefault, "buttonDefault");
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.UseVisualStyleBackColor = true;
            this.buttonDefault.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // groupBoxTypeChart
            // 
            resources.ApplyResources(this.groupBoxTypeChart, "groupBoxTypeChart");
            this.groupBoxTypeChart.Controls.Add(this.comboBoxTypeChart);
            this.groupBoxTypeChart.Name = "groupBoxTypeChart";
            this.groupBoxTypeChart.TabStop = false;
            // 
            // comboBoxTypeChart
            // 
            resources.ApplyResources(this.comboBoxTypeChart, "comboBoxTypeChart");
            this.comboBoxTypeChart.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApp1.Properties.Settings.Default, "ComboBoxTypeChartText", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.comboBoxTypeChart.FormattingEnabled = true;
            this.comboBoxTypeChart.Items.AddRange(new object[] {
            resources.GetString("comboBoxTypeChart.Items"),
            resources.GetString("comboBoxTypeChart.Items1"),
            resources.GetString("comboBoxTypeChart.Items2")});
            this.comboBoxTypeChart.Name = "comboBoxTypeChart";
            this.comboBoxTypeChart.Text = global::WindowsFormsApp1.Properties.Settings.Default.ComboBoxTypeChartText;
            this.comboBoxTypeChart.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // colorDialogIfSort
            // 
            this.colorDialogIfSort.Color = global::WindowsFormsApp1.Properties.Settings.Default.ColumsIfSort;
            // 
            // colorDialogSortedColums
            // 
            this.colorDialogSortedColums.Color = global::WindowsFormsApp1.Properties.Settings.Default.SortedColums;
            // 
            // Main_Settings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBoxTypeChart);
            this.Controls.Add(this.buttonDefault);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAppy);
            this.Controls.Add(this.groupBoxColor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main_Settings";
            this.Load += new System.EventHandler(this.Compare_Settings_Load);
            this.groupBoxColor.ResumeLayout(false);
            this.groupBoxColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSortedColums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIfColums)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxNotSortColums)).EndInit();
            this.groupBoxTypeChart.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button buttonNotSortColums;
        private System.Windows.Forms.ColorDialog colorDialogColumsNotSort;
        private System.Windows.Forms.GroupBox groupBoxColor;
        private System.Windows.Forms.Button buttonAppy;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelSortedColums;
        private System.Windows.Forms.Button buttonSortedColums;
        private System.Windows.Forms.Label labelIfColums;
        private System.Windows.Forms.Button buttonIfColums;
        private System.Windows.Forms.Label labelNotSortColums;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.PictureBox pictureBoxNotSortColums;
        private System.Windows.Forms.PictureBox pictureBoxSortedColums;
        private System.Windows.Forms.PictureBox pictureBoxIfColums;
        private System.Windows.Forms.ColorDialog colorDialogIfSort;
        private System.Windows.Forms.ColorDialog colorDialogSortedColums;
        private System.Windows.Forms.Button buttonDefault;
        private System.Windows.Forms.GroupBox groupBoxTypeChart;
        private System.Windows.Forms.ComboBox comboBoxTypeChart;
    }
}