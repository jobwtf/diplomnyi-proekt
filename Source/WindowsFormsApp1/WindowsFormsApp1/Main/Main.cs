﻿using System;
using System.Windows.Forms;
using CefSharp.WinForms;

namespace WindowsFormsApp1
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
        ChromiumWebBrowser chrome;
        ChromiumWebBrowser chrome_browser;

        private void Form1_Load(object sender, EventArgs e)
        {
            //BackColor = Properties.Settings.Default.FormColor;
            //ForeColor = Properties.Settings.Default.FontColor;
            //richTextBoxTheory.BackColor= Properties.Settings.Default.FormColor;
            //richTextBoxTheory.ForeColor = Properties.Settings.Default.FontColor;
            //comboBoxSort.BackColor = Properties.Settings.Default.FormColor;
            //comboBoxSort.ForeColor = Properties.Settings.Default.FontColor;
            //treeViewProgLanguage.BackColor = Properties.Settings.Default.FormColor;
            //treeViewProgLanguage.ForeColor = Properties.Settings.Default.FontColor;
            //richTextBoxProgLanguage.BackColor = Properties.Settings.Default.FormColor;
            //richTextBoxProgLanguage.ForeColor = Properties.Settings.Default.FontColor;
            //menuStripMain.BackColor = Properties.Settings.Default.FormColor;
            //menuStripMain.ForeColor = Properties.Settings.Default.FontColor;

            chrome = new ChromiumWebBrowser("");
            this.YoutubePlayer.Controls.Add(chrome);
            chrome_browser = new ChromiumWebBrowser("");
            this.WebBrowser.Controls.Add(chrome_browser);
        }

        private void сравненияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Compare compare = new Compare();
            compare.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //            Пузырьковая сортировка
            //Шейкерная сортировка
            //Сортировка вставками
            //Сортировка выбором
            //Сортировка слиянием
            //Сортировка шелла
            //https://youtu.be/oqpICiM165I

            switch (comboBoxSort.SelectedIndex)
            {
                case 0: //Пузырьковый
                    richTextBoxTheory.LoadFile(@"Information\buble sort\theory\file.txt", RichTextBoxStreamType.UnicodePlainText);
                    chrome_browser.Load("http://catcut.net/l9rt");
                    chrome.Load("https://youtube.com/embed/oqpICiM165I?rel=0&autoplay=0&fs=0");
                    break;
                case 1: //Шейкерный
                    richTextBoxTheory.LoadFile(@"Information\shake sort\theory\file.txt", RichTextBoxStreamType.UnicodePlainText);
                    chrome_browser.Load("http://catcut.net/zKst");
                    chrome.Load("https://www.youtube.com/embed/V773gjY2kuI?rel=0&autoplay=0&fs=0");
                    break;
                case 2: //Вставками
                    richTextBoxTheory.LoadFile(@"Information\insert sort\theory\file.txt", RichTextBoxStreamType.UnicodePlainText);
                    chrome_browser.Load("http://catcut.net/uott");
                    chrome.Load("https://www.youtube.com/embed/dAAkElskMmU?rel=0&autoplay=0&fs=0");
                    break;
                case 3://Выбором
                    richTextBoxTheory.LoadFile(@"Information\selection sort\theory\file.txt", RichTextBoxStreamType.UnicodePlainText);
                    chrome_browser.Load("http://catcut.net/Qott");
                    chrome.Load("https://www.youtube.com/embed/8Y89DRq9Y3o?rel=0&autoplay=0&fs=0");
                    break;
                case 4://Слиянием
                    richTextBoxTheory.LoadFile(@"Information\merge sort\theory\file.txt", RichTextBoxStreamType.UnicodePlainText);
                    chrome_browser.Load("http://catcut.net/Vott");
                    chrome.Load("https://www.youtube.com/embed/BaY1a0VPIsE?rel=0&autoplay=0&fs=0");
                    break;
                case 5://Шелла
                    richTextBoxTheory.LoadFile(@"Information\shell sort\theory\file.txt", RichTextBoxStreamType.UnicodePlainText);
                    chrome_browser.Load("http://catcut.net/CLQu");
                    chrome.Load("https://www.youtube.com/embed/lvts84Qfo8o?rel=0&autoplay=0&fs=0");
                    break;
            }
        }

        private void treeViewProgLanguage_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var comboIndex = new string[] { "buble sort", "shake sort","insert sort", "selection sort", "merge sort","shell sort"  };
            if (Convert.ToString(treeViewProgLanguage.SelectedNode.Tag) != "main")
            {
                try
                {
                    var fileName = System.IO.Path.Combine("Information", comboIndex[comboBoxSort.SelectedIndex], "code", Convert.ToString(treeViewProgLanguage.SelectedNode.Tag));
                    richTextBoxProgLanguage.LoadFile(fileName, RichTextBoxStreamType.UnicodePlainText);
                }
                catch (Exception)
                {
                    richTextBoxProgLanguage.Text = "Выберете сортировку!";
                }
            }

        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.Show();
        }
    }
}
