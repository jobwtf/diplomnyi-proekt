﻿namespace WindowsFormsApp1
{
    partial class Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("C");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("C#");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("C++");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Java");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Delphi/Pascal");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Python");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Языки программирования", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6});
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.сравненияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.comboBoxSort = new System.Windows.Forms.ComboBox();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageTeor = new System.Windows.Forms.TabPage();
            this.richTextBoxTheory = new System.Windows.Forms.RichTextBox();
            this.tabPageWiki = new System.Windows.Forms.TabPage();
            this.WebBrowser = new System.Windows.Forms.Panel();
            this.tabPageYoutube = new System.Windows.Forms.TabPage();
            this.YoutubePlayer = new System.Windows.Forms.Panel();
            this.tabPageCodeSort = new System.Windows.Forms.TabPage();
            this.richTextBoxProgLanguage = new System.Windows.Forms.RichTextBox();
            this.treeViewProgLanguage = new System.Windows.Forms.TreeView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuStripMain.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPageTeor.SuspendLayout();
            this.tabPageWiki.SuspendLayout();
            this.tabPageYoutube.SuspendLayout();
            this.tabPageCodeSort.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.сравненияToolStripMenuItem,
            this.оПрограммеToolStripMenuItem});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(800, 24);
            this.menuStripMain.TabIndex = 0;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // сравненияToolStripMenuItem
            // 
            this.сравненияToolStripMenuItem.Name = "сравненияToolStripMenuItem";
            this.сравненияToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.сравненияToolStripMenuItem.Text = "Сравнения";
            this.сравненияToolStripMenuItem.Click += new System.EventHandler(this.сравненияToolStripMenuItem_Click);
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // comboBoxSort
            // 
            this.comboBoxSort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSort.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboBoxSort.FormattingEnabled = true;
            this.comboBoxSort.Items.AddRange(new object[] {
            "Пузырьковая сортировка",
            "Шейкерная сортировка",
            "Сортировка вставками",
            "Сортировка выбором",
            "Сортировка слиянием",
            "Сортировка Шелла"});
            this.comboBoxSort.Location = new System.Drawing.Point(12, 27);
            this.comboBoxSort.Name = "comboBoxSort";
            this.comboBoxSort.Size = new System.Drawing.Size(776, 21);
            this.comboBoxSort.TabIndex = 1;
            this.comboBoxSort.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tabControlMain
            // 
            this.tabControlMain.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControlMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlMain.Controls.Add(this.tabPageTeor);
            this.tabControlMain.Controls.Add(this.tabPageWiki);
            this.tabControlMain.Controls.Add(this.tabPageYoutube);
            this.tabControlMain.Controls.Add(this.tabPageCodeSort);
            this.tabControlMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tabControlMain.Location = new System.Drawing.Point(12, 54);
            this.tabControlMain.Multiline = true;
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(776, 393);
            this.tabControlMain.TabIndex = 1;
            // 
            // tabPageTeor
            // 
            this.tabPageTeor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.tabPageTeor.Controls.Add(this.richTextBoxTheory);
            this.tabPageTeor.Location = new System.Drawing.Point(25, 4);
            this.tabPageTeor.Name = "tabPageTeor";
            this.tabPageTeor.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageTeor.Size = new System.Drawing.Size(747, 385);
            this.tabPageTeor.TabIndex = 0;
            this.tabPageTeor.Text = "Теория";
            this.tabPageTeor.UseVisualStyleBackColor = true;
            // 
            // richTextBoxTheory
            // 
            this.richTextBoxTheory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxTheory.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxTheory.Name = "richTextBoxTheory";
            this.richTextBoxTheory.ReadOnly = true;
            this.richTextBoxTheory.Size = new System.Drawing.Size(741, 379);
            this.richTextBoxTheory.TabIndex = 0;
            this.richTextBoxTheory.Text = resources.GetString("richTextBoxTheory.Text");
            // 
            // tabPageWiki
            // 
            this.tabPageWiki.Controls.Add(this.WebBrowser);
            this.tabPageWiki.Location = new System.Drawing.Point(25, 4);
            this.tabPageWiki.Name = "tabPageWiki";
            this.tabPageWiki.Size = new System.Drawing.Size(747, 385);
            this.tabPageWiki.TabIndex = 2;
            this.tabPageWiki.Text = "Wikipedia";
            this.tabPageWiki.UseVisualStyleBackColor = true;
            // 
            // WebBrowser
            // 
            this.WebBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WebBrowser.Location = new System.Drawing.Point(0, 0);
            this.WebBrowser.Name = "WebBrowser";
            this.WebBrowser.Size = new System.Drawing.Size(747, 385);
            this.WebBrowser.TabIndex = 0;
            // 
            // tabPageYoutube
            // 
            this.tabPageYoutube.Controls.Add(this.YoutubePlayer);
            this.tabPageYoutube.Location = new System.Drawing.Point(25, 4);
            this.tabPageYoutube.Name = "tabPageYoutube";
            this.tabPageYoutube.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageYoutube.Size = new System.Drawing.Size(747, 385);
            this.tabPageYoutube.TabIndex = 1;
            this.tabPageYoutube.Text = "Youtube";
            this.tabPageYoutube.UseVisualStyleBackColor = true;
            // 
            // YoutubePlayer
            // 
            this.YoutubePlayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.YoutubePlayer.Location = new System.Drawing.Point(3, 3);
            this.YoutubePlayer.Name = "YoutubePlayer";
            this.YoutubePlayer.Size = new System.Drawing.Size(741, 379);
            this.YoutubePlayer.TabIndex = 1;
            // 
            // tabPageCodeSort
            // 
            this.tabPageCodeSort.Controls.Add(this.richTextBoxProgLanguage);
            this.tabPageCodeSort.Controls.Add(this.treeViewProgLanguage);
            this.tabPageCodeSort.Location = new System.Drawing.Point(25, 4);
            this.tabPageCodeSort.Name = "tabPageCodeSort";
            this.tabPageCodeSort.Size = new System.Drawing.Size(747, 385);
            this.tabPageCodeSort.TabIndex = 4;
            this.tabPageCodeSort.Text = "Код сортировки";
            this.tabPageCodeSort.UseVisualStyleBackColor = true;
            // 
            // richTextBoxProgLanguage
            // 
            this.richTextBoxProgLanguage.Location = new System.Drawing.Point(202, 3);
            this.richTextBoxProgLanguage.Name = "richTextBoxProgLanguage";
            this.richTextBoxProgLanguage.Size = new System.Drawing.Size(521, 377);
            this.richTextBoxProgLanguage.TabIndex = 1;
            this.richTextBoxProgLanguage.Text = "";
            // 
            // treeViewProgLanguage
            // 
            this.treeViewProgLanguage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeViewProgLanguage.Location = new System.Drawing.Point(0, 0);
            this.treeViewProgLanguage.Name = "treeViewProgLanguage";
            treeNode1.Name = "C";
            treeNode1.Tag = "c.txt";
            treeNode1.Text = "C";
            treeNode2.Name = "C#";
            treeNode2.Tag = "c#.txt";
            treeNode2.Text = "C#";
            treeNode3.Name = "C++";
            treeNode3.Tag = "c++.txt";
            treeNode3.Text = "C++";
            treeNode4.Name = "Java";
            treeNode4.Tag = "java.txt";
            treeNode4.Text = "Java";
            treeNode5.Name = "Delphi";
            treeNode5.Tag = "delphi.txt";
            treeNode5.Text = "Delphi/Pascal";
            treeNode6.Name = "Python";
            treeNode6.Tag = "python.txt";
            treeNode6.Text = "Python";
            treeNode7.Name = "ProgrammingLanguage";
            treeNode7.Tag = "main";
            treeNode7.Text = "Языки программирования";
            this.treeViewProgLanguage.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode7});
            this.treeViewProgLanguage.Size = new System.Drawing.Size(747, 385);
            this.treeViewProgLanguage.TabIndex = 0;
            this.treeViewProgLanguage.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewProgLanguage_AfterSelect);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.comboBoxSort);
            this.Controls.Add(this.menuStripMain);
            this.Controls.Add(this.tabControlMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripMain;
            this.Name = "Main";
            this.Text = "Система изучения методов сортировки";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.tabControlMain.ResumeLayout(false);
            this.tabPageTeor.ResumeLayout(false);
            this.tabPageWiki.ResumeLayout(false);
            this.tabPageYoutube.ResumeLayout(false);
            this.tabPageCodeSort.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBoxSort;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageTeor;
        private System.Windows.Forms.TabPage tabPageYoutube;
        private System.Windows.Forms.TabPage tabPageWiki;
        private System.Windows.Forms.ToolStripMenuItem сравненияToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Panel YoutubePlayer;
        private System.Windows.Forms.TabPage tabPageCodeSort;
        private System.Windows.Forms.TreeView treeViewProgLanguage;
        private System.Windows.Forms.RichTextBox richTextBoxProgLanguage;
        private System.Windows.Forms.RichTextBox richTextBoxTheory;
        private System.Windows.Forms.Panel WebBrowser;
    }
}

