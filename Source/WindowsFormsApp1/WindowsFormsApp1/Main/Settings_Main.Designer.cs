﻿namespace WindowsFormsApp1
{
    partial class Settings_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSizeFont = new System.Windows.Forms.GroupBox();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonAppy = new System.Windows.Forms.Button();
            this.groupBoxColor = new System.Windows.Forms.GroupBox();
            this.pictureFontColor = new System.Windows.Forms.PictureBox();
            this.pictureFormColor = new System.Windows.Forms.PictureBox();
            this.labelFontColor = new System.Windows.Forms.Label();
            this.buttonFontColor = new System.Windows.Forms.Button();
            this.labelColorForm = new System.Windows.Forms.Label();
            this.buttonFormColor = new System.Windows.Forms.Button();
            this.colorDialogForm = new System.Windows.Forms.ColorDialog();
            this.colorDialogFont = new System.Windows.Forms.ColorDialog();
            this.comboBoxFontSize = new System.Windows.Forms.ComboBox();
            this.groupBoxSizeFont.SuspendLayout();
            this.groupBoxColor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFontColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFormColor)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxSizeFont
            // 
            this.groupBoxSizeFont.Controls.Add(this.comboBoxFontSize);
            this.groupBoxSizeFont.Location = new System.Drawing.Point(12, 105);
            this.groupBoxSizeFont.Name = "groupBoxSizeFont";
            this.groupBoxSizeFont.Size = new System.Drawing.Size(283, 50);
            this.groupBoxSizeFont.TabIndex = 13;
            this.groupBoxSizeFont.TabStop = false;
            this.groupBoxSizeFont.Text = "Размер шрифта";
            // 
            // buttonDefault
            // 
            this.buttonDefault.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonDefault.Location = new System.Drawing.Point(250, 220);
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.Size = new System.Drawing.Size(75, 23);
            this.buttonDefault.TabIndex = 12;
            this.buttonDefault.Text = " Сброс настроек";
            this.buttonDefault.UseVisualStyleBackColor = true;
            this.buttonDefault.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // buttonOk
            // 
            this.buttonOk.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonOk.Location = new System.Drawing.Point(7, 220);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 11;
            this.buttonOk.Text = "Ок";
            this.buttonOk.UseVisualStyleBackColor = true;
            this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonCancel.Location = new System.Drawing.Point(169, 220);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 10;
            this.buttonCancel.Text = "Отмена";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonAppy
            // 
            this.buttonAppy.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonAppy.Location = new System.Drawing.Point(88, 220);
            this.buttonAppy.Name = "buttonAppy";
            this.buttonAppy.Size = new System.Drawing.Size(75, 23);
            this.buttonAppy.TabIndex = 9;
            this.buttonAppy.Text = "Применить";
            this.buttonAppy.UseVisualStyleBackColor = true;
            this.buttonAppy.Click += new System.EventHandler(this.buttonAppy_Click);
            // 
            // groupBoxColor
            // 
            this.groupBoxColor.Controls.Add(this.pictureFontColor);
            this.groupBoxColor.Controls.Add(this.pictureFormColor);
            this.groupBoxColor.Controls.Add(this.labelFontColor);
            this.groupBoxColor.Controls.Add(this.buttonFontColor);
            this.groupBoxColor.Controls.Add(this.labelColorForm);
            this.groupBoxColor.Controls.Add(this.buttonFormColor);
            this.groupBoxColor.Location = new System.Drawing.Point(12, 12);
            this.groupBoxColor.Name = "groupBoxColor";
            this.groupBoxColor.Size = new System.Drawing.Size(283, 87);
            this.groupBoxColor.TabIndex = 8;
            this.groupBoxColor.TabStop = false;
            this.groupBoxColor.Text = "Изменение цветовой палитры";
            // 
            // pictureFontColor
            // 
            this.pictureFontColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureFontColor.Location = new System.Drawing.Point(171, 53);
            this.pictureFontColor.Name = "pictureFontColor";
            this.pictureFontColor.Size = new System.Drawing.Size(17, 17);
            this.pictureFontColor.TabIndex = 7;
            this.pictureFontColor.TabStop = false;
            // 
            // pictureFormColor
            // 
            this.pictureFormColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.pictureFormColor.Location = new System.Drawing.Point(171, 24);
            this.pictureFormColor.Name = "pictureFormColor";
            this.pictureFormColor.Size = new System.Drawing.Size(17, 17);
            this.pictureFormColor.TabIndex = 6;
            this.pictureFormColor.TabStop = false;
            // 
            // labelFontColor
            // 
            this.labelFontColor.AutoSize = true;
            this.labelFontColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelFontColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelFontColor.Location = new System.Drawing.Point(6, 53);
            this.labelFontColor.Name = "labelFontColor";
            this.labelFontColor.Size = new System.Drawing.Size(41, 13);
            this.labelFontColor.TabIndex = 3;
            this.labelFontColor.Text = "Шрифт";
            // 
            // buttonFontColor
            // 
            this.buttonFontColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonFontColor.Location = new System.Drawing.Point(202, 48);
            this.buttonFontColor.Name = "buttonFontColor";
            this.buttonFontColor.Size = new System.Drawing.Size(75, 23);
            this.buttonFontColor.TabIndex = 2;
            this.buttonFontColor.Text = "Изменить";
            this.buttonFontColor.UseVisualStyleBackColor = true;
            this.buttonFontColor.Click += new System.EventHandler(this.buttonFontColor_Click);
            // 
            // labelColorForm
            // 
            this.labelColorForm.AutoSize = true;
            this.labelColorForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.labelColorForm.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.labelColorForm.Location = new System.Drawing.Point(6, 24);
            this.labelColorForm.Name = "labelColorForm";
            this.labelColorForm.Size = new System.Drawing.Size(61, 13);
            this.labelColorForm.TabIndex = 1;
            this.labelColorForm.Text = "Фон форм";
            // 
            // buttonFormColor
            // 
            this.buttonFormColor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.buttonFormColor.Location = new System.Drawing.Point(202, 19);
            this.buttonFormColor.Name = "buttonFormColor";
            this.buttonFormColor.Size = new System.Drawing.Size(75, 23);
            this.buttonFormColor.TabIndex = 0;
            this.buttonFormColor.Text = "Изменить";
            this.buttonFormColor.UseVisualStyleBackColor = true;
            this.buttonFormColor.Click += new System.EventHandler(this.buttonFormColor_Click);
            // 
            // comboBoxFontSize
            // 
            this.comboBoxFontSize.DataBindings.Add(new System.Windows.Forms.Binding("Text", global::WindowsFormsApp1.Properties.Settings.Default, "ComboBoxFontSize", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.comboBoxFontSize.FormattingEnabled = true;
            this.comboBoxFontSize.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14"});
            this.comboBoxFontSize.Location = new System.Drawing.Point(9, 20);
            this.comboBoxFontSize.Name = "comboBoxFontSize";
            this.comboBoxFontSize.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFontSize.TabIndex = 0;
            this.comboBoxFontSize.Text = global::WindowsFormsApp1.Properties.Settings.Default.ComboBoxFontSize;
            this.comboBoxFontSize.SelectedIndexChanged += new System.EventHandler(this.comboBoxTypeChart_SelectedIndexChanged);
            // 
            // Settings_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 255);
            this.Controls.Add(this.groupBoxSizeFont);
            this.Controls.Add(this.buttonDefault);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonAppy);
            this.Controls.Add(this.groupBoxColor);
            this.Name = "Settings_Main";
            this.Text = "Settings_Main";
            this.Load += new System.EventHandler(this.Settings_Main_Load);
            this.groupBoxSizeFont.ResumeLayout(false);
            this.groupBoxColor.ResumeLayout(false);
            this.groupBoxColor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFontColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureFormColor)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSizeFont;
        private System.Windows.Forms.ComboBox comboBoxFontSize;
        private System.Windows.Forms.Button buttonDefault;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonAppy;
        private System.Windows.Forms.GroupBox groupBoxColor;
        private System.Windows.Forms.PictureBox pictureFontColor;
        private System.Windows.Forms.PictureBox pictureFormColor;
        private System.Windows.Forms.Label labelFontColor;
        private System.Windows.Forms.Button buttonFontColor;
        private System.Windows.Forms.Label labelColorForm;
        private System.Windows.Forms.Button buttonFormColor;
        private System.Windows.Forms.ColorDialog colorDialogForm;
        private System.Windows.Forms.ColorDialog colorDialogFont;
    }
}