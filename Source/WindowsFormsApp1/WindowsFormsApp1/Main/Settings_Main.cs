﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Settings_Main : Form
    {

        public Settings_Main()
        {          
            InitializeComponent();
        }

        private void Settings_Main_Load(object sender, EventArgs e)
        {
            buttonAppy.Enabled = false;
            pictureFormColor.BackColor = Properties.Settings.Default.FormColor;
            pictureFontColor.BackColor = Properties.Settings.Default.FontColor;
            BackColor = Properties.Settings.Default.FormColor;
            ForeColor = Properties.Settings.Default.FontColor;
        }

        private void buttonFormColor_Click(object sender, EventArgs e)
        {
            if (colorDialogForm.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.FormColor = colorDialogForm.Color;
                pictureFormColor.BackColor = colorDialogForm.Color;
                buttonAppy.Enabled = true;
            }
        }

        private void buttonFontColor_Click(object sender, EventArgs e)
        {
            if (colorDialogFont.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.FontColor = colorDialogFont.Color;
                pictureFontColor.BackColor = colorDialogFont.Color;
                buttonAppy.Enabled = true;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Reset();
            Close();
        }

        private void comboBoxTypeChart_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (comboBoxFontSize.SelectedItem)
            {
                case "8":
                    {
                        Properties.Settings.Default.FontSize = 8;
                        Properties.Settings.Default.ComboBoxFontSize = "8";
                        buttonAppy.Enabled = true;
                    }
                    break;

                case "9":
                    {
                        Properties.Settings.Default.FontSize = 9;
                        Properties.Settings.Default.ComboBoxFontSize = "9";
                        buttonAppy.Enabled = true;
                    }
                    break;

                case "10":
                    {
                        Properties.Settings.Default.FontSize = 10;
                        Properties.Settings.Default.ComboBoxFontSize = "10";
                        buttonAppy.Enabled = true;
                    }
                    break;
                case "11":
                    {
                        Properties.Settings.Default.FontSize = 11;
                        Properties.Settings.Default.ComboBoxFontSize = "11";
                        buttonAppy.Enabled = true;
                    }
                    break;
                case "12":
                    {
                        Properties.Settings.Default.FontSize = 12;
                        Properties.Settings.Default.ComboBoxFontSize = "12";
                        buttonAppy.Enabled = true;
                    }
                    break;
                case "13":
                    {
                        Properties.Settings.Default.FontSize = 13;
                        Properties.Settings.Default.ComboBoxFontSize = "13";
                        buttonAppy.Enabled = true;
                    }
                    break;
                case "14":
                    {
                        Properties.Settings.Default.FontSize = 14;
                        Properties.Settings.Default.ComboBoxFontSize = "14";
                        buttonAppy.Enabled = true;
                    }
                    break;

            }
        }

        private void buttonDefault_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.FormColor = DefaultBackColor;
            Properties.Settings.Default.FontColor = Color.Black;
            Properties.Settings.Default.FontSize = 8;
            comboBoxFontSize.Text = "8";
            pictureFormColor.BackColor = DefaultBackColor;
            pictureFontColor.BackColor = Color.Black;
            Properties.Settings.Default.Save();
        }

        private void buttonAppy_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            buttonAppy.Enabled = false;
        }

        private void buttonOk_Click(object sender, EventArgs e)
        {
            MessageBox.Show("При изменения главных настроек, требуется перезапустить всю программу!");
            Close();
        }
    }
}
